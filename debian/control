Source: qdjango
Section: libs
Priority: optional
Maintainer: Jeremy Lainé <jeremy.laine@m4x.org>
Build-Depends: debhelper (>= 9), qtbase5-dev, libqt5sql5-sqlite
Build-Depends-Indep: doxygen
Standards-Version: 4.2.1
Homepage: https://github.com/jlaine/qdjango
Vcs-Git: https://salsa.debian.org/debian/qdjango.git
Vcs-Browser: https://salsa.debian.org/debian/qdjango

Package: libqdjango-db0
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Description: Database library for the QDjango framework
 QDjango is a cross-platform C++ web development framework built upon Qt.
 Where possible it tries to follow django's API, hence its name.
 .
 This package contains the database object relational model library.

Package: libqdjango-http0
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Description: HTTP library for the QDjango framework
 QDjango is a cross-platform C++ web development framework built upon Qt.
 Where possible it tries to follow django's API, hence its name.
 .
 This package contains the HTTP library.

Package: libqdjango-dbg
Section: debug
Architecture: any
Depends: ${misc:Depends}, libqdjango-db0 (= ${binary:Version}), libqdjango-http0 (= ${binary:Version})
Multi-Arch: same
Description: Debugging symbols for the QDjango framework
 QDjango is a cross-platform C++ web development framework built upon Qt.
 Where possible it tries to follow django's API, hence its name.
 .
 This package contains the debugging symbols.

Package: libqdjango-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends}, libqdjango-db0 (= ${binary:Version}), libqdjango-http0 (= ${binary:Version}), qtbase5-dev
Suggests: libqdjango-doc
Multi-Arch: same
Description: Development files for the QDjango framework
 QDjango is a cross-platform C++ web development framework built upon Qt.
 Where possible it tries to follow django's API, hence its name.
 .
 This package contains the development headers and libraries.

Package: libqdjango-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}, libjs-jquery, lynx | www-browser
Suggests: libqdjango-dev
Description: Documentation for the QDjango framework
 QDjango is a cross-platform C++ web development framework built upon Qt.
 Where possible it tries to follow django's API, hence its name.
 .
 This package contains the HTML documentation.

